import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.ml.feature.FeatureHasher;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.collection.Seq;

import java.io.*;

public class JavaApp {
    public static void main(String[] args) throws IOException {

        Utils.sendErrToFile();

        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]")
                .setAppName("spark-app-v3");
        SparkContext sc = SparkContext.getOrCreate(sparkConf);
        SparkSession sparkSession = new SparkSession(sc);

        String filePath = args[0];

        Dataset<Row> ds = sparkSession.read()
                .format("com.databricks.spark.csv")
                .option("header", "true")
                .option("inferschema", "true")
                .load(filePath);

        String[] colNames = ds.schema().fieldNames();

        ds.printSchema();

        Seq<String> targetColumns = null;
        try {
            targetColumns = new DataParser().parseColumns(colNames);
        } catch (IOException e) {
            e.printStackTrace();
            sc.stop();
            sparkSession.stop();
            System.exit(1);
        }

        System.out.println("type country or regex for model");

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));

        String countryOrRegex = reader.readLine(); // TODO: change to args[]

        Dataset<Row> ds1 = ds
                .where(colNames[0] + " rlike '" + countryOrRegex + "'")
                .selectExpr(targetColumns);
        System.out.println("rows in ds1: " + ds1.count());

        String labelCol = targetColumns.head();
        targetColumns = targetColumns.drop(1).toSeq();
        System.out.println("Label: " + labelCol);
        System.out.println("Features: " + targetColumns.toString());

        FeatureHasher hasher = new FeatureHasher()
                .setInputCols(targetColumns)
                .setOutputCol("features");

        Dataset<Row> featurized = hasher.transform(ds1);
        Dataset<Row> ds2 = featurized.selectExpr(labelCol + " as label", "features");
        ds2.show(1, false);

        //-------------------------------------------------------------------------------

        Dataset<Row>[] splits = ds2.randomSplit(new double[]{0.7, 0.3}, 3L);
        Dataset<Row> training = splits[0].cache();
        Dataset<Row> testing = splits[1];
        training.cache();

        new RegressionModelBuilder().buildRegression(training, testing, 100);

        new Visualiser().showChart(testing, 0, "test data");

        sc.stop();
        sparkSession.stop();
    }
}
