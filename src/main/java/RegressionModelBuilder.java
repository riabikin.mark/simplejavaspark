import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionSummary;
import org.apache.spark.ml.regression.LinearRegressionTrainingSummary;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class RegressionModelBuilder {

    public void buildRegression(Dataset<Row> training, Dataset<Row> testing
            , int iter)
    {
        LinearRegression lr = new LinearRegression()
                .setMaxIter(iter)
                .setRegParam(0.3)
                .setElasticNetParam(0.8);

        System.out.println("Training...");
        // TODO: async training
        org.apache.spark.ml.regression.LinearRegressionModel lrModel = lr.fit(training);

        System.out.println("Coefficients: "
                + lrModel.coefficients() + " Intercept: " + lrModel.intercept());
        LinearRegressionTrainingSummary trainingSummary = lrModel.summary();
        System.out.println("Total iterations " + trainingSummary.totalIterations());
        System.out.println("\n    SELF evaluation");
        System.out.println("numIterations: " + trainingSummary.totalIterations());
        //System.out.println("objectiveHistory: " + Vectors.dense(trainingSummary.objectiveHistory()));
        //trainingSummary.residuals().show();
        System.out.println("RMSE: " + trainingSummary.rootMeanSquaredError());
        System.out.println("r2: " + trainingSummary.r2());

        LinearRegressionSummary testSummary = lrModel.evaluate(testing);
        System.out.println("\n    on TEST data");
        System.out.println("RMSE: " + testSummary.rootMeanSquaredError());
        System.out.println("r2: " + testSummary.r2());
        //testSummary.predictions().show(false);
    }
}
