useful links:

https://github.com/apache/spark/tree/master/examples/src/main/java/org/apache/spark/examples

https://github.com/PacktPublishing/Machine-Learning-End-to-Endguide-for-Java-developers

https://spark.apache.org/docs/latest/ml-features.html

http://spark.apache.org/docs/latest/ml-pipeline.html

https://www.programcreek.com/java-api-examples/index.php?api=org.apache.spark.sql.Row

https://www.programcreek.com/java-api-examples/index.php?api=org.apache.spark.sql.api.java.Row

https://stackoverflow.com/questions/41116694/how-to-apply-map-function-on-dataset-in-spark-java

https://issues.apache.org/jira/browse/SPARK-15465

https://stackoverflow.com/questions/50571810/spark-countvectorizer-return-a-tinyint

https://www.programcreek.com/java-api-examples/index.php?api=org.apache.spark.mllib.regression.LinearRegressionWithSGD

https://spark.apache.org/docs/latest/ml-classification-regression.html

https://spark.apache.org/docs/2.3.0/mllib-evaluation-metrics.html

https://spark.apache.org/docs/2.3.0/mllib-linear-methods.html

https://medium.com/@achilleus/spark-session-10d0d66d1d24

