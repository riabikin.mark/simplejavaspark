import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Visualiser {

    public void showChart(Dataset<Row> rowDataset, int featureNumber
            , String chartName) {

        JavaRDD<Row> d = rowDataset.javaRDD();
        d.cache();

        JavaRDD<LabeledPoint> p = d.map(r -> {
            String[] parts = r.toString()
                    .replaceAll("[\\]|\\)|\\[]", "")
                    .split("\\(");
            parts[0] = parts[0].replaceAll(",", "");
            String[] features = parts[1].split(",");
            double[] v = new double[(features.length-1)/2];
            for (int i = 0; i < v.length; i++) {
                v[i] = Double.valueOf(features[i + (features.length-1)/2+1]).doubleValue();
            }
            return new LabeledPoint(Double.valueOf(parts[0]).doubleValue(), Vectors.dense(v));
        });
        p.cache();

        System.out.println("Feature vector size: " + p.first().features().size());
        List<Double> xx = p.collect().stream().map(LabeledPoint::label)
                .collect(Collectors.toList());
        List<Double> yy = p.collect().stream().map(labeledPoint ->
                labeledPoint.features().toArray()[featureNumber])
                .collect(Collectors.toList());

        XYChart chart = new XYChart(700, 600, Styler.ChartTheme.GGPlot2);
        XYSeries series = chart.addSeries(chartName, xx, yy);
        series.setLineWidth((float) 0.0001);
        series.setMarkerColor(Color.BLUE);

        new SwingWrapper(chart).displayChart();
    }
}
