import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Vector;

public class DataParser {

    public Seq<String> parseColumns(String[] colNames) throws IOException {

        System.out.println("Please, input Label column number(from zer0)");

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        Vector<String> targetCols = new Vector<>(2);
        //targetCols.add("country_name"); targetCols.add("time");
        targetCols.add(colNames[Integer.parseInt(input)]);

        System.out.println("input features columns numbers(from zer0)" +
                "split by ','");

        input = reader.readLine();
        String[] inputArray = input.split(",");

        for (int i = 0; i < inputArray.length; i++) {
            targetCols.add(colNames[Integer.parseInt(inputArray[i])]);
        }

        return JavaConverters
                .asScalaIteratorConverter(targetCols.subList(0, targetCols.size())
                        .iterator()).asScala().toSeq();
    }

}
