import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Utils {

    public static void sendErrToFile() {
        FileOutputStream fs;
        PrintStream out;
        try {
            fs = new FileOutputStream("target\\ERR_output.txt");
            out = new PrintStream(fs);
            System.setErr(out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
